% Author: Aleksandra Tomaszowska

clear;

r_d = 0.002;    % radius of the wire in m
r_z = 5.08*10^-2;   % radius of the coil in m 
m = 10^-2;  %the mass in kg
s_t = 2;    % time constant in s

% F_0 = m*g 	driving force
g = 9.81;   % alpha= F_0/m in m/s^2
G = 80*10^9;    % shear modulus in Pa 
N=2:1:50;   % number of coils (from 2 to 50)
omega=50:0.5:400;    % range of angular frequencies 

% creating the new file
file_ID = fopen('resonance_analysis_results.dat','w');
% writng in the file constants
fprintf(file_ID,'m= %.2fkg  r_d= %.0dm  r_z= %.3fm  g= %.2f m/s^2   tau= %ds  G= %.2d Pa ',m,r_d,r_z,g,s_t,G);
fprintf(file_ID,'\n\n\n');

beta = 1/2*s_t;  % damping coefficient in Hz
for i=1:size(N,2); % for every N counting resonance amplitude 
    
  k = (G * r_d^4)/(4 * N(i) * r_z^3);
  omega_0 = sqrt(k/m);
  A(i) = g / sqrt((4 * omega_0^2 * beta^2));
  % writng in the file all numbers of coil, maximum amplitudes and ranges of angular frequencies
  fprintf(file_ID,'N= %d  A= %.3f m   f= %.3f Hz  \n',N(i),A(i),omega_0); %printing the data to the file
  
end
% create plot
subplot(2,1,2);
plot(N,A);
grid on;
grid minor;
title('Maximum Amplitude in function of Number of coils');
xlabel('N');
ylabel('A-,m');
xlim([5 50]);

% closing the file
fclose(file_ID);

for i = 1:size(N,2);% for every number of coil

    k = (G * r_d^4)/(4 * N(i) * r_z^3); % counting spring constant 
    omega_0 = sqrt(k/m); % counting omega_0
    
 % for constant value of N count amplitude for all frequencies
    for j = 1:size(omega,2);  
    
        A(j,i) = g/sqrt((omega_0^2 - omega(j)^2)^2 + 4*beta^2*omega(j)^2);
        
    end
    
    % plotting the curves 
    
    subplot(2,1,1);
    f=omega/2/pi;
    plot(f,A);
    grid on;
    grid minor;
    xlabel('f ,Hz'); 
    xlim([5 65]);
    ylabel('A,m');
    


end
saveas(gcf,'resonance.pdf'); % creating PDF file with the 2 graphs
